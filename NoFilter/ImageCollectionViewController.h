//
//  ImageCollectionViewController.h
//  NoFilter
//
//  Created by Robert on 1/14/15.
//
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewController : UICollectionViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>
typedef void (^ResizeBlock)(void);
@end
