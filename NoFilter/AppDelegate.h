//
//  AppDelegate.h
//  NoFilter
//
//  Created by Robert on 1/14/15.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

