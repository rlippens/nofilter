//
//  ImageCollectionViewController.m
//  NoFilter
//
//  Created by Robert on 1/14/15.
//
//

#import "ImageCollectionViewController.h"
#import "ImageCollectionViewCell.h"
#import "UIImage+NFExtensions.h"
#import <Photos/Photos.h>

@interface ImageCollectionViewController ()
@property (strong, nonatomic) NSMutableArray *imagesArray;
@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (strong, nonatomic) NSOperationQueue *operationQueue;
@end

@implementation ImageCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(chooseImage:)];
    self.imagesArray = [NSMutableArray new];
    self.operationQueue = [NSOperationQueue new];
    self.operationQueue.name = @"Image Rescaling";
    self.operationQueue.maxConcurrentOperationCount = 4;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [self.collectionView reloadData];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.imagesArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    // Configure the cell
    __block UIImage *cellImage = [self.imagesArray objectAtIndex:indexPath.row];
    
    ResizeBlock resizeImageBlock = ^
    {
        [cell.imageLoadingSpinner startAnimating];
        cellImage = [cellImage scaledImageConstrainedToSize:CGSizeMake(70, 70)];
        [cell.imageLoadingSpinner stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.photoImage.image = cellImage;
        });
    };
    
    [self.operationQueue addOperationWithBlock:resizeImageBlock];
    return cell;
}

#pragma mark Image Selection Logic
- (IBAction)chooseImage:(id)sender {
    UIAlertController *popUpChoices = [[UIAlertController alloc] init];
    UIAlertAction *cameraChoiceAction = [UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectImageFromChosenSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    UIAlertAction *photoLibraryChoiceAction = [UIAlertAction actionWithTitle:@"Take Photo or Video" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectImageFromChosenSource:UIImagePickerControllerSourceTypeCamera];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [popUpChoices addAction:cameraChoiceAction];
    [popUpChoices addAction:photoLibraryChoiceAction];
    [popUpChoices addAction:cancelAction];
    [self presentViewController:popUpChoices animated:YES completion:nil];
}


-(void)selectImageFromChosenSource:(enum UIImagePickerControllerSourceType)sourceType {
    self.imagePicker = [UIImagePickerController new];
    self.imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        self.imagePicker.sourceType = sourceType;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    } else {
        UIAlertController *unavailableSourceTypeAlert = [UIAlertController alertControllerWithTitle:@"Unavailable" message:@"The desired source type is unavailable" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [unavailableSourceTypeAlert dismissViewControllerAnimated:YES completion:nil];
        }];
        [unavailableSourceTypeAlert addAction:okAction];
        [self presentViewController:unavailableSourceTypeAlert animated:YES completion:nil];
    }
}

#pragma mark <UIImagePickerControllerDelegate>
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [self.imagesArray addObject:image];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self.collectionView reloadData];
    }];
}

@end
