//
//  ImageCollectionViewCell.h
//  NoFilter
//
//  Created by Robert on 1/15/15.
//
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imageLoadingSpinner;
-(instancetype)initWithImage:(UIImageView*)imageView;
@end
