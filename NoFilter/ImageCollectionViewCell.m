//
//  ImageCollectionViewCell.m
//  NoFilter
//
//  Created by Robert on 1/15/15.
//
//

#import "ImageCollectionViewCell.h"

@implementation ImageCollectionViewCell
-(instancetype)initWithImage:(UIImageView*)imageView {
    if (self = [super init]) {
        self.photoImage = imageView;
    }
    return self;
}
@end
